#include <iostream>
#include<string>
#include<list>
#include <iterator>

using namespace std;
list<string>spisok;
class List {
private:
list<string>refund_elem;
public:
	
	void push_back(string item) { spisok.push_back(item); }
	
	void print() {
		if (spisok.empty()) {
			cout << "Cписок не содержит элементов для вывода.\nКоманда выполена.\nУкажите другую команду: ";
		}
		else {
			cout << "Список\n";
			int i = 1;
			list<string>::iterator it = spisok.begin();
			for (it = spisok.begin(); it != spisok.end(); it++) {
				cout << i++ << ". " << *it << endl;
			}
			cout << "Вы успешно вывели список.\nКоманда выполнена.\nУкажите другую команду: ";
		}
	}
	void pop_back() { 
		if (spisok.empty()) {
			cout << "Cписок не содержит элементов для очистки.\nКоманда не выполена.\nУкажите другую команду: ";
		}
		else {
			spisok.pop_back();
			cout << "Вы удалили последний элемент списка.\nВведите новую команду: ";
		}
	}
	
	void clear() { 
		if (spisok.empty()) {
			cout << "Список уже является чистым.\nКоманда не выполена.\nУкажите другую команду: ";
		}
		else {
			spisok.clear();
			cout << "Список очищен.\nВведите новую команду: ";
		}
	}
	
	void reverse() {
		if (spisok.empty()) {
			cout << "Список не содержит элементов для преобразования.\nКоманда не выполена.\nУкажите другую команду: ";
		}
		else if (spisok.begin() == spisok.end()) {
			cout << "Список содержит один элемент.\nКоманда не выполнена.\nУкажите другую команду: ";
		}
		else {
			spisok.reverse();
			cout << "Преобразование в обратном порядке выполнено.\nВведите новую команду: ";
		}
	}
	void search(string search) {
		int ch = 0,i=1;
		list<string>::iterator it = spisok.begin();
		for (it = spisok.begin(); it != spisok.end(); it++) {
			
			if (*it == search) {
				cout <<i<<". " << *it;
				ch++;
			}
			i++;
		}
		if (ch == 0) {
			cout << "Запрос не найден в списке.\nСкорректируйте запрос или введите новую команду.\nКоманда успешно выполнена, поиск произведён.\nВведите новую команду: ";
		}
	}
  void search2(int num){
    if(spisok.empty()){
      cout<<"Список не содержит элементов";
      return;
    }
    int i=0;
    copy(spisok.begin(), spisok.end(), inserter(refund_elem, refund_elem.begin()));
    refund_elem.sort();
    list<string>::iterator it = refund_elem.begin();
			for (it = refund_elem.begin(); it != refund_elem.end(); it++) {
        i++;
        if(i==num){
          cout <<i<< ". " << *it << endl;
          refund_elem.clear();
          return;
        }
			}
  }
};
int main() {
	setlocale(LC_ALL, "rus");
	cout << "Список команд:"<<endl;
	cout << " 1.add - добавить элемент в конец списка.\n";
	cout << " 2.del - удалить последний элемент списка.\n";
	cout << " 3.clear - полная очистка списка.\n";
	cout << " 4.print - вывод списка на экран.\n";
	cout << " 5.reverse - преобразование списка в обратном порядке.\n";
	cout << " 6.search - поиск элемента по содержанию.\n";
  cout << " 7.search_id - вывод элемента по индеку.\n";
	cout << "Введите команду, которую вы желаете выполнить: ";
	while (true) {
		List a;
		int i = 1;
		string command;
		cin >> command;
		if (command == "add") {
			cout << "Процесс добавления элементов в список не прерывый, для завершения введите команду 'stop'\n";
			while (true) {
				cout << "Введите элемент для заполнения списка: "<<i++<<".";
				string push;
				cin >> push;
				
				if (push == "stop") {
					i--;
					cout << "Вы успешно покинули раздел заполнения списка.\nВведите новую команду: ";
					break;
				}
				else {
					
					a.push_back(push);
				}
			}
		}
		else if(command=="del" ) {
			i--;
			a.pop_back();
		}
		else if (command == "clear") {
			i=1;
			a.clear();			
		}
		else if (command == "print") {
			a.print();
		}
		else if (command == "reverse") {
			a.reverse();
		}
		else if (command == "search") {
			string search;
			cout << "Введите слово для поиска";
			cin >> search;
			a.search(search);
		}
    else if (command == "search_id") {
      cout<<"Укажите номер элемента для вывода: ";
      int num;
      cin>>num;
			a.search2(num);
		}
		else {
			cout << "Вы указали неккоректную команду.\nКоманда не выполнена.\nВведите другую команду: ";
		}
	}
}
